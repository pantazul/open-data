## Piano d'azione per l'Open Data comunale


Risutato di co-progettazione con i 5 Comuni dell'Umbria che attuano politiche di Agenda Urbana: Città di Castello, Foligno, Perugia, Terni, Spoleto.

---

### Introduzione all'Open Data - definizione e benefici

- Valore economico: i riutilizzatori (aziende, sviluppatori,etc.) possono realizzare analisi basate sui dati (ad esempio le elaborazioni dati possono aiutare a capire meglio le opportunità di investimento: analizzando la distribuzione degli esercizi commerciali presenti, della popolazione, dei trasporti, dei servizi pubblici si può decidere se e come investire), oppure possono utilizzarli per realizzare nuovi servizi (App, chatBot, etc.) ed attuare nuovi modelli di business. 

- Valore sociale: l'Open Data permette di conseguire finalità di miglioramento della trasparenza e dell'affidabilità dell'operato delle PA e potenzialmente abilita i processi di partecipazione (ad esempio mettere a disposizione i dati di cui i cittadini hanno effettivo bisogno).

- Valore di performance: una PA ha necessità di gestire al proprio interno i dati e nel suo operato è previsto lo scambio dati tra settori diversi della stessa amministrazione o tra amministrazioni. Mettere i dati a disposizione in Open Data può comportare una riorganizzazione dei processi interni di gestione dei dati, con beneficio per l'operatività quotidiana e conseguente riduzione dei costi. Il tempo e le risorse che si impiegano per fornire dati a fronte di una richiesta da parte di un cittadino, pensare ad esempio alla normativa F.O.I.A., o di un altro ufficio, possono venire ottimizzati se si attuano meccanismi di pubblicazione in Open Data. Esporre i dati può portare un miglioramento nella qualità del dato stesso, che si cercherà di rendere più completo, aggiornato e corretto possibile, a beneficio di utenti interni ed esterni.

---


### Open Data - aspetti normativi

- Riferimenti dettagliati a Norme Europee - CAD - - L.R. 2014 - MOOD Umbria Enti Locali

- Trasparenza e Open Data

- Foia e Open Data

- Elenco dei dati che un comune è obbligato a pubblicare e normativa


---

### Costruire una strategia Open Data per il proprio Comune

---

### Raccomandazioni su diffusione e comunicazione

---

### Raccomandazioni sul processo di gestione dei dati aperti

---

### Raccomandazioni tecniche su come processare i dati

---

### Quali dati

---

### I dataset chiave

---

### Conclusioni

- TEMA | Dataset | Ente che si occupa della pubblicazione | ANNO |
- anagrafe edifici pubblici e ad uso pubblico | Elenco, localizzazione e caratteristiche degli edifici | Comune | 1 |
- lo stradario comunale - toponomastica comunale | Stradario | Comune | 1 |
- punti di interesse e infrastrutture | Aree Verdi | Comune | 1 |
- punti di interesse e infrastrutture | Cammini e percorsi (sia sentieristica che itineriari di visita) | Comune | 1 |
- punti di interesse e infrastrutture | WI-FI|Regione Umbria / Comune se ha gestione del dato non condivisa con R.U. | 1 |
- orari del trasporto locale | Entità GTFS | Regione | 1 |
- mobilità/viabilità | Mappa Aree ZTL | Comune | 1 |
- mobilità/viabilità | Localizzazione dei Varchi ZTL e orari | Comune | 1 |
- mobilità/viabilità | Localizzazione e caratteristiche dei parcheggi | Comune | 1 |
- scuole | Localizzazione e caratteristiche | Comune se nella propria disponibilità | 1 |
- mense scolastiche | menu delle mense | Comune | 1 |
- dati dal piano di Protezione Civile | Punti di raccolta , Vie di fuga, Aree di ammassamento, ... | Comune | 1 |


---

![Flux Explained](https://facebook.github.io/flux/img/flux-simple-f8-diagram-explained-1300w.png)


---